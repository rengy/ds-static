package cn.rengy.dsstaticdemo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service 
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {  
		UserDetails userDetails=null;
		if(username.equals("admin")) {
			userDetails = User.withUsername(username) 
					.password(passwordEncoder.encode("zA!RVZa3Vb@Q")).authorities("ROLE_ADMIN")
					.build(); 
		}else if(username.equals("test")) {
			userDetails = User.withUsername(username) 
					.password(passwordEncoder.encode("JMYdN$4OFTXq")).authorities("ROLE_TEST")
					.build(); 
		}
		//String hashpw ="{noop}123456";
		
		return userDetails;
	}
	
}