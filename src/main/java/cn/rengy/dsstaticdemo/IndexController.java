package cn.rengy.dsstaticdemo;
 
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
/**
 *
 */
@Controller
public class IndexController {
 
	
	@RequestMapping(value = "/")
    public String home() {
        return "redirect:/index";
    }
	
	/**
	 * 登录页
	 * @return
	 */
	@RequestMapping(value = "/index")
    public ModelAndView index() {
        ModelAndView mv=new ModelAndView();
        mv.setViewName("/login/index");
        return mv;
    }
	/**
	 * 登录成功页
	 * @return
	 */
    @RequestMapping(value = "/admin")
    public ModelAndView say() {
        ModelAndView mv=new ModelAndView();
        
        Authentication user=SecurityContextHolder.getContext().getAuthentication();
        String username=user.getName();
        mv.addObject("username",username);
        mv.setViewName("/kzht/index");
        return mv;
    }
    
    @RequestMapping(value = "/kzhtIndex")
    public ModelAndView kzhtIndex() {
        ModelAndView mv=new ModelAndView();
        
        Authentication user=SecurityContextHolder.getContext().getAuthentication();
        String username=user.getName();
        mv.addObject("username",username);
        mv.setViewName("/kzht/index3");
        return mv;
    }
    
    /**
	 * 管理页 只有admin能访问
	 * @return
	 */
    @RequestMapping(value = "/admin/gl")
    @Secured("ROLE_ADMIN")
    public ModelAndView admingl() {
        ModelAndView mv=new ModelAndView();
        
        Authentication user=SecurityContextHolder.getContext().getAuthentication();
        String username=user.getName();
        mv.addObject("username",username);
        mv.setViewName("/gl/index");
        return mv;
    }
   
    /**
	 * 计算机页
	 * @return
	 */
    @RequestMapping(value = "/admin/jsj")
    public ModelAndView jsj() {
        ModelAndView mv=new ModelAndView();
        
        Authentication user=SecurityContextHolder.getContext().getAuthentication();
        String username=user.getName();
        mv.addObject("username",username);
        mv.setViewName("/jsj/index");
        return mv;
    }
}