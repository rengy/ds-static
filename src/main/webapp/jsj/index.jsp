<%@ page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html><html data-scrapbook-source="https://10.209.135.108:4119/Application.screen?#computers_root" data-scrapbook-create="20211119135058822" data-scrapbook-title="亚信安全服务器深度安全防护系统管理中心" data-scrapbook-type="site"><head><meta charset="UTF-8">
	<title>亚信安全服务器深度安全防护系统管理中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
		<script src="/jsj/lib/bootstrap/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="/jsj/bootstrap-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/jsj/tmStatusBar-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/jsj/splitter-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/jsj/tmDropDownList-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/jsj/tmAccordion-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/jsj/tmNavigator-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/styles-zh_CN-1065112.csa.css">

	

  </head>

  <body style="overflow: hidden; background-color: #ddd;">
	


























<div id="tm_header" class="unselectable cfx">
	<a id="tm_logo_link"><img class="tm_logo" src="/jsj/product_logo_zh_CN.svg"></a>

	

		<ul id="tm_header_links" data-dropdown="dropdown">

		
		<li class="dropdown">
			<span href="javascript:" class="">
				<strong id="signedInUsername">${username}</strong>

			</span>
			
		</li>
		<li>
			<a style="cursor: pointer;color: #fff;" href="/logout">
				注销
			</a>
		</li>
		</ul>

	

	<div id="nav_div" class="unselectable wrapped"><nav><ul id="tm_navigator_ul">
	<li id="tab_dashboard" class="tab"><a class="tab_link" href="/admin">控制台</a></li>
	
	<!-- <li id="tab_alerts" class="tab"><a class="tab_link" href="javascript:">警报</a></li>
	<li id="tab_eventsAndReports" class="tab"><a class="tab_link" href="javascript:">事件和报告</a></li> -->
	
	<li id="tab_computers" class="tab selected">
	<a class="tab_link" href="javascript:">计算机</a></li>
	<!-- <li id="tab_policies" class="tab"><a class="tab_link" href="javascript:">策略</a></li>
	<li id="tab_edrview" class="tab"><a class="tab_link" href="javascript:">检测响应</a></li>
	<li id="tab_asmview" class="tab"><a class="tab_link" href="javascript:">资产发现</a></li>
	<li id="tab_hostreinforcement" class="tab"><a class="tab_link" href="javascript:">主机加固</a></li> -->
	
	
	<% String username=(String)request.getAttribute("username");if(username.equals("admin")){%>	
					<li id="tab_administration" class="tab"><a class="tab_link"
						href="/admin/gl">管理</a></li>
					<% }%>
	
	</ul></nav></div>
</div>




	<!--  insert any alert bars -->
	<div id="alert-bar">
		
		
		
		
		
		
	</div>	
	
 	<div id="split-area" class="container_splitter" style="">
    	<div id="splitter" class="splitter s_cfx" style="height: 588.4px;">
			<div class="sidebar" style="width: 190px;">
	            <div class="contentLeft spinner_large">
	            <div id="contextMenu" class="contextmenu_box" style="min-width:180px;position:absolute;display:none;z-index:100;">

			
			
			
			<div id="addComputerContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_addComputerContextItem_img" alt="" src="/jsj/servers.svg" width="18" height="18">&nbsp;添加计算机...</div>

			<div id="addDirectoryContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_addDirectoryContextItem_img" alt="" src="/jsj/ad_add_directory.svg" width="18" height="18">&nbsp;添加 Active Directory...</div>

			<div id="addVirtualContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_addVirtualContextItem_img" alt="" src="/jsj/vcenters_top.svg" width="18" height="18">&nbsp;添加 Virtual Center...</div>

			<div id="addAWSMenuItem" class="contextmenu_item" style="display:none;"><img id="tbi_addAWSMenuItem_img" alt="" src="/jsj/aws_account.svg" width="18" height="18">&nbsp;添加 AWS 帐户...</div>

			<div id="addAzureMenuItem" class="contextmenu_item" style="display:none;"><img id="tbi_addAzureMenuItem_img" alt="" src="/jsj/azure_top.svg" width="18" height="18">&nbsp;添加 Azure 帐户...</div>

			<div id="addVCloudMenuItem" class="contextmenu_item" style="display:none;"><img id="tbi_addVCloudMenuItem_img" alt="" src="/jsj/vcloud_top.svg" width="18" height="18">&nbsp;添加 vCloud 帐户...</div>

			
			<div class="contextmenu_spacer"></div>

			<div id="addGroupContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_addGroupContextItem_img" alt="" src="/jsj/folder.svg" width="18" height="18">&nbsp;创建组...</div>

			<div class="contextmenu_spacer"></div>

			
			<div id="deleteGroupContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deleteGroupContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除组</div>

			<div class="contextmenu_spacer"></div>

			<div id="addSmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_addSmartFolderItem_img" alt="" src="/jsj/smart_folder.svg" width="18" height="18">&nbsp;创建智能文件夹...</div>
			
			<div class="contextmenu_spacer"></div>

			<div id="moveToGroupContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_moveToGroupContextItem_img" alt="" src="/jsj/move_to_group.svg" width="18" height="18">&nbsp;移动计算机...</div>

			<div id="removeDirectoryContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_removeDirectoryContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除目录...</div>

			<div id="removeVirtualContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_removeVirtualContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除 Virtual Center...</div>


			<div id="synchronizeDirContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_synchronizeDirContextItem_img" alt="" src="/jsj/sync.svg" width="18" height="18">&nbsp;立即同步</div>

			<div id="synchronizeVirContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_synchronizeVirContextItem_img" alt="" src="/jsj/sync.svg" width="18" height="18">&nbsp;立即同步</div>


			<div id="removeCloudProviderContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_removeCloudProviderContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除云帐户...</div>

			<div id="synchronizeCloudProviderContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_synchronizeCloudProviderContextItem_img" alt="" src="/jsj/sync.svg" width="18" height="18">&nbsp;立即同步</div>

			<div id="convertToAmazonAccountContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_convertToAmazonAccountContextItem_img" alt="" src="/jsj/aws_account.svg" width="18" height="18">&nbsp;升级到 AWS 帐户...</div>


			<div id="removeAmazonAccountContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_removeAmazonAccountContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除 Amazon 帐户...</div>

			<div id="synchronizeAmazonAccountContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_synchronizeAmazonAccountContextItem_img" alt="" src="/jsj/sync.svg" width="18" height="18">&nbsp;立即同步</div>


			<div id="directoryCleanupContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_directoryCleanupContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;目录清理...</div>

			
			<div class="contextmenu_spacer"></div>

			
			<div id="propertiesContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_propertiesContextItem_img" alt="" src="/jsj/details.svg" width="18" height="18">&nbsp;属性...</div>

			
			<div id="addSubSmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_addSubSmartFolderItem_img" alt="" src="/jsj/smart_folder.svg" width="18" height="18">&nbsp;创建子智能文件夹...</div>

			<div id="copySmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_copySmartFolderItem_img" alt="" src="/jsj/smart_folder_copy.svg" width="18" height="18">&nbsp;复制智能文件夹...</div>

			<div id="synchronizeSmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_synchronizeSmartFolderItem_img" alt="" src="/jsj/sync.svg" width="18" height="18">&nbsp;同步智能文件夹</div>

			<div id="deleteSmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_deleteSmartFolderItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;删除智能文件夹...</div>

			<div id="editSmartFolderItem" class="contextmenu_item" style="display:none;"><img id="tbi_editSmartFolderItem_img" alt="" src="/jsj/details.svg" width="18" height="18">&nbsp;智能文件夹属性...</div>

				
		</div>


		<form name="mainForm" method="post" action="https://10.209.135.108:4119/ComputersMenu.screen" id="mainForm" class=""><input id="rID" name="rID" type="hidden" value="EA3D9CFD338C05F6348BC5397C187D73"><input id="cmdArguments" name="cmdArguments" type="hidden" value=""><input id="command" name="command" type="hidden" value=""><input id="changed" name="changed" type="hidden" value="false">
			<input id="menuHash" name="menuHash" type="hidden" value="-569110402">
			<div id="mainMessage" class="alert-message" style="display:none;height:1px;overflow:hidden;"><table style="margin-bottom:0px;"><tbody><tr><td style="width:24px"><img id="mainMessage_icon" src="/jsj/blank.gif" style="vertical-align:middle" alt="" width="24" height="24"> </td><td style="padding-left: 10px;"><span id="mainMessage_text"></span></td><td style="width:24px"><a id="mainMessage_close" class="close" href="#">×</a></td></tr></tbody></table></div><div id="mainMessage_ghost" class="alert-message" style="position:absolute;top:-1000px;left:-1000px;z-index:999997;"></div> 

			<div id="mainContent" style="">
				<div id="menu">
<div id="menu_root" class="menutreeview_item_row"><div id="menu_root_div" class="menutreeview_item_selected" style="padding-left:8px;"><a href="javascript:" style="outline: 0;"><img id="menu_root_brancher" src="/jsj/tree_close.svg" width="18" height="18"></a><span id="menu_root_text" tabindex="0" class="menutreeview_item_text"><img src="/jsj/servers.svg" alt="" width="18" height="18"><span>计算机</span></span><div class="context-caret btn" style="display: none;">
<img src="/jsj/ellipsis.svg" width="16px" height="16px"></div></div></div>
</div>
<input type="hidden" id="menu_viewstate" name="menu_viewstate" value="root,SmartFolderRoot">
<input type="hidden" id="menu_selected" name="menu_selected" value="root">


	        </div>
		<div class="formFooter" style="display:none">
</div>
</form>
	            </div>
			</div><div class="splitbarV" unselectable="on" style="cursor: e-resize; user-select: none;"><span></span><div style="cursor: pointer;" class="splitbuttonV" unselectable="on"></div></div>
			<div class="content spinner_large" style="width: 1336px;"> <!-- The content frame was default with a spinner -->
			<div id="contextMenu" class="contextmenu_box" style="min-width:auto;position:absolute;display:none;z-index:100;">

			<div id="collapseAllGroupsContextItem" class="contextmenu_item" style="width:auto;"><img id="tbi_collapseAllGroupsContextItem_img" alt="" src="/jsj/tree_close.svg" width="18" height="18">&nbsp;全部折叠</div>

			<div id="expandAllGroupsContextItem" class="contextmenu_item" style="width:auto;"><img id="tbi_expandAllGroupsContextItem_img" alt="" src="/jsj/tree_open.svg" width="18" height="18">&nbsp;全部展开</div>

			<div id="selectAllContextItem" class="contextmenu_item" style="width:auto;"><img id="tbi_selectAllContextItem_img" alt="" src="/jsj/blank.gif" width="18" height="18">&nbsp;全选 (100)</div>

			<div class="contextmenu_spacer"></div>

			<div id="exportCSVSelectedContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_exportCSVSelectedContextItem_img" alt="" src="/jsj/export.svg" width="18" height="18">&nbsp;将选定内容导出为 CSV...</div>

			<div id="exportXMLSelectedContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_exportXMLSelectedContextItem_img" alt="" src="/jsj/export.svg" width="18" height="18">&nbsp;将选定内容导出为 XML (供导入)...</div>

			<div class="contextmenu_spacer"></div>

			<div id="actionsContextItem" class="contextmenu_item" style="width:auto;"><div style="display:table; width: auto;"><div style="display:row;"><div style="display:table-cell;width:99%;" class="contextmenu_item_text"><img id="tbi_actionsContextItem_img" alt="" src="/jsj/action.svg" width="18" height="18">&nbsp;操作</div><div class="contextmenu_expander" style="display:table-cell;"><div>&nbsp;</div></div></div></div></div>

			<div id="viewContextItem" class="contextmenu_item" style="width:auto;"><div style="display:table; width: auto;"><div style="display:row;"><div style="display:table-cell;width:99%;" class="contextmenu_item_text"><img id="tbi_viewContextItem_img" alt="" src="/jsj/view.svg" width="18" height="18">&nbsp;事件</div><div class="contextmenu_expander" style="display:table-cell;"><div>&nbsp;</div></div></div></div></div>

			<div class="contextmenu_spacer"></div>

			<div id="deleteContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deleteContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;删除...</div>

			<div class="contextmenu_spacer"></div>

			<div id="detailsContextItem" class="contextmenu_item" style="width:auto;"><img id="tbi_detailsContextItem_img" alt="" src="/jsj/details.svg" width="18" height="18">&nbsp;<b>详细信息...</b></div>

		</div>


		
		
		<div id="viewMenu" class="contextmenu_box" style="width:auto;position:absolute;display:none;z-index:101;">

			
			
				<div id="hostEventsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_hostEventsContextItem_img" alt="" src="/jsj/servers.svg" width="18" height="18">&nbsp;系统事件...</div>

			
				<div id="com_trendmicro_ds_antimalware_antiMalwareContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareContextItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--anti_malware.asvg.svg" width="18" height="18">&nbsp;防恶意软件事件...</div>

			
				<div id="com_trendmicro_ds_antimalware_webReputationContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_antimalware_webReputationContextItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--web_reputation.asvg.svg" width="18" height="18">&nbsp;Web 信誉事件...</div>

			
				<div id="com_trendmicro_ds_network_packetLogsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_network_packetLogsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--firewall.asvg.svg" width="18" height="18">&nbsp;防火墙事件...</div>

			
				<div id="com_trendmicro_ds_network_payloadLogsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_network_payloadLogsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--dpi.asvg.svg" width="18" height="18">&nbsp;入侵防御事件...</div>

			
				<div id="com_trendmicro_ds_integrity_integrityEventsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_integrity_integrityEventsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--integrity_monitoring.asvg.svg" width="18" height="18">&nbsp;完整性监控事件...</div>

			
				<div id="com_trendmicro_ds_loginspection_logInspectionEventsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_loginspection_logInspectionEventsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.loginspection-images-icons--log_inspection.asvg.svg" width="18" height="18">&nbsp;日志审查事件...</div>

			
				<div id="com_trendmicro_ds_appcontrol_appControlEventsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_appcontrol_appControlEventsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control.asvg.svg" width="18" height="18">&nbsp;应用程序控制事件...</div>

			
		</div>



		<div id="toolbarDropMenuView" class="toolbardropmenu_box" style="width:auto;position:absolute;display:none;top:65px;left:365px;z-index:100;">

			
			
				<div id="hostEventsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_hostEventsToolbarDropMenuItem_img" alt="" src="/jsj/servers.svg" style="vertical-align:top;" width="18" height="18">&nbsp;系统事件...</div>

			
				<div id="com_trendmicro_ds_antimalware_antiMalwareToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--anti_malware.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;防恶意软件事件...</div>

			
				<div id="com_trendmicro_ds_antimalware_webReputationToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_antimalware_webReputationToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--web_reputation.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;Web 信誉事件...</div>

			
				<div id="com_trendmicro_ds_network_packetLogsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_network_packetLogsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--firewall.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;防火墙事件...</div>

			
				<div id="com_trendmicro_ds_network_payloadLogsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_network_payloadLogsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--dpi.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;入侵防御事件...</div>

			
				<div id="com_trendmicro_ds_integrity_integrityEventsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_integrity_integrityEventsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--integrity_monitoring.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;完整性监控事件...</div>

			
				<div id="com_trendmicro_ds_loginspection_logInspectionEventsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_loginspection_logInspectionEventsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.loginspection-images-icons--log_inspection.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;日志审查事件...</div>

			
				<div id="com_trendmicro_ds_appcontrol_appControlEventsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_appcontrol_appControlEventsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;应用程序控制事件...</div>

			
		</div>

	
		
		
				
		
		<div id="actionsMenu" class="contextmenu_box" style="width:auto;position:absolute;display:none;z-index:101;">

			
			
					<div id="deployContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deployContextItem_img" alt="" src="/jsj/deploy_agent.svg" width="18" height="18">&nbsp;部署客户端</div>

					
					<div id="undeployContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_undeployContextItem_img" alt="" src="/jsj/remove_agent.svg" width="18" height="18">&nbsp;移除客户端</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="activateAgentContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_activateAgentContextItem_img" alt="" src="/jsj/activate_reactivate.svg" width="18" height="18">&nbsp;激活/重新激活</div>

					
					<div id="checkStatusContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_checkStatusContextItem_img" alt="" src="/jsj/check_status.svg" width="18" height="18">&nbsp;检查状态</div>

					
					<div id="deactivateAgentContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deactivateAgentContextItem_img" alt="" src="/jsj/deactivate.svg" width="18" height="18">&nbsp;停用</div>

					
					<div id="restoreEsxContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_restoreEsxContextItem_img" alt="" src="/jsj/esx_server_restore.svg" width="18" height="18">&nbsp;移除过滤器驱动程序...</div>

					
					<div id="deployAgentlessContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deployAgentlessContextItem_img" alt="" src="/jsj/deploy_appliance.svg" width="18" height="18">&nbsp;部署无客户端安全...</div>

					
					<div id="unDeployAgentlessContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_unDeployAgentlessContextItem_img" alt="" src="/jsj/delete.svg" width="18" height="18">&nbsp;移除客户端</div>

					
					<div id="upgradeFastPathDriverContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_upgradeFastPathDriverContextItem_img" alt="" src="/jsj/icon_upgrade_filter_driver.png" width="18" height="18">&nbsp;升级过滤器驱动程序...</div>

					
					<div id="activateApplianceContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_activateApplianceContextItem_img" alt="" src="/jsj/activate_appliance.svg" width="18" height="18">&nbsp;激活设备...</div>

					
					<div id="activateNSXApplianceContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_activateNSXApplianceContextItem_img" alt="" src="/jsj/activate_appliance.svg" width="18" height="18">&nbsp;激活/重新激活</div>

					
					<div id="deactivateApplianceContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_deactivateApplianceContextItem_img" alt="" src="/jsj/deactivate_appliance.svg" width="18" height="18">&nbsp;停用设备</div>

					
					<div id="upgradeApplianceContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_upgradeApplianceContextItem_img" alt="" src="/jsj/upgrade_appliance.svg" width="18" height="18">&nbsp;升级设备...</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="updateNowContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_updateNowContextItem_img" alt="" src="/jsj/send_policy.svg" width="18" height="18">&nbsp;发送策略</div>

					
					<div id="cancelUpdateContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_cancelUpdateContextItem_img" alt="" src="/jsj/cancel_send_policy.svg" width="18" height="18">&nbsp;取消“发送策略”</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="componentUpdateContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_componentUpdateContextItem_img" alt="" src="/jsj/update_components.svg" width="18" height="18">&nbsp;下载安全更新</div>

					
					<div id="componentRollbackContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_componentRollbackContextItem_img" alt="" src="/jsj/rollback_components.svg" width="18" height="18">&nbsp;还原安全更新</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="getEventsNowContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_getEventsNowContextItem_img" alt="" src="/jsj/get_events.svg" width="18" height="18">&nbsp;获取事件</div>

					
					<div id="clearWarningsAndErrorsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_clearWarningsAndErrorsContextItem_img" alt="" src="/jsj/clear_warning_error.svg" width="18" height="18">&nbsp;清除警告/错误</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="upgradeAgentSoftwareContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_upgradeAgentSoftwareContextItem_img" alt="" src="/jsj/upgrade_agent_software.svg" width="18" height="18">&nbsp;升级客户端软件...</div>

					
					<div id="cancelUpgradeContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_cancelUpgradeContextItem_img" alt="" src="/jsj/cancel_upgrade_agent_software.svg" width="18" height="18">&nbsp;取消升级客户端软件</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="scanForRecommendationsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_scanForRecommendationsContextItem_img" alt="" src="/jsj/scan_for_recommendations.svg" width="18" height="18">&nbsp;漏洞扫描 (推荐设置)</div>

					
					<div id="cancelRecommendationScanContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_cancelRecommendationScanContextItem_img" alt="" src="/jsj/cancel_scan_for_recommendation.svg" width="18" height="18">&nbsp;取消“漏洞扫描 (推荐设置)”</div>

					
					<div id="clearRecommendationsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_clearRecommendationsContextItem_img" alt="" src="/jsj/clear_recommendation.svg" width="18" height="18">&nbsp;清除建议</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareQuickScanContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareQuickScanContextItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--quick_scan_for_malware.asvg.svg" width="18" height="18">&nbsp;快速扫描恶意软件</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareManualScanContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareManualScanContextItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--full_scan_for_malware.asvg.svg" width="18" height="18">&nbsp;完整扫描恶意软件</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareCancelScanContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareCancelScanContextItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--abort_scan_for_malware.asvg.svg" width="18" height="18">&nbsp;取消恶意软件扫描</div>

					
					<div id="com_trendmicro_ds_network_scanForOpenPortsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_network_scanForOpenPortsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--port_scan.asvg.svg" width="18" height="18">&nbsp;扫描打开的端口</div>

					
					<div id="com_trendmicro_ds_network_cancelScanForOpenPortsContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_network_cancelScanForOpenPortsContextItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--port_scan_cancel.asvg.svg" width="18" height="18">&nbsp;取消扫描打开的端口</div>

					
					<div id="com_trendmicro_ds_integrity_scanForSystemIntegrityContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_integrity_scanForSystemIntegrityContextItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--scan_for_integrity.asvg.svg" width="18" height="18">&nbsp;扫描完整性</div>

					
					<div id="com_trendmicro_ds_integrity_rebuildBaselineContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_integrity_rebuildBaselineContextItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--rebuild_computer_baseline.asvg.svg" width="18" height="18">&nbsp;重新生成完整性基线</div>

					
					<div id="com_trendmicro_ds_integrity_scanPasswordContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_integrity_scanPasswordContextItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--scanweakpassword.asvg.svg" width="18" height="18">&nbsp;扫描弱口令</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlTurnOffMaintenanceModeContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_appcontrol_appControlTurnOffMaintenanceModeContextItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" width="18" height="18">&nbsp;关闭维护模式</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlTurnOnMaintenanceModeContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_appcontrol_appControlTurnOnMaintenanceModeContextItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" width="18" height="18">&nbsp;打开维护模式...</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlExtendMaintenanceModeContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_com_trendmicro_ds_appcontrol_appControlExtendMaintenanceModeContextItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" width="18" height="18">&nbsp;修改维护模式...</div>

					 <div class="contextmenu_spacer"></div>
 
					<div id="moveToGroupContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_moveToGroupContextItem_img" alt="" src="/jsj/move_to_group.svg" width="18" height="18">&nbsp;移动到组...</div>

					
					<div id="assignSecurityProfileContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_assignSecurityProfileContextItem_img" alt="" src="/jsj/assign_policy.svg" width="18" height="18">&nbsp;分配策略...</div>

					
					<div id="assignHostAssetImportanceContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_assignHostAssetImportanceContextItem_img" alt="" src="/jsj/assign_asset_value.svg" width="18" height="18">&nbsp;分配资产值...</div>

					
					<div id="assignHostHistoryIAUContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_assignHostHistoryIAUContextItem_img" alt="" src="/jsj/historyupdate.svg" width="18" height="18">&nbsp;分配历史更新源...</div>

					
					<div id="assignRelayListContextItem" class="contextmenu_item" style="display:none;"><img id="tbi_assignRelayListContextItem_img" alt="" src="/jsj/assign_relay_group.svg" width="18" height="18">&nbsp;分配中继组...</div>

					
		</div>

		
		
		<div id="toolbarDropMenuActions" class="toolbardropmenu_box" style="width:auto;position:absolute;display:none;top:65px;left:365px;z-index:100;">

			
			
					<div id="deployToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_deployToolbarDropMenuItem_img" alt="" src="/jsj/deploy_agent.svg" style="vertical-align:top;" width="18" height="18">&nbsp;部署客户端</div>

					
					<div id="undeployToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_undeployToolbarDropMenuItem_img" alt="" src="/jsj/remove_agent.svg" style="vertical-align:top;" width="18" height="18">&nbsp;移除客户端</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="activateAgentToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_activateAgentToolbarDropMenuItem_img" alt="" src="/jsj/activate_reactivate.svg" style="vertical-align:top;" width="18" height="18">&nbsp;激活/重新激活</div>

					
					<div id="checkStatusToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_checkStatusToolbarDropMenuItem_img" alt="" src="/jsj/check_status.svg" style="vertical-align:top;" width="18" height="18">&nbsp;检查状态</div>

					
					<div id="deactivateAgentToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_deactivateAgentToolbarDropMenuItem_img" alt="" src="/jsj/deactivate.svg" style="vertical-align:top;" width="18" height="18">&nbsp;停用</div>

					
					<div id="restoreEsxToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_restoreEsxToolbarDropMenuItem_img" alt="" src="/jsj/esx_server_restore.svg" style="vertical-align:top;" width="18" height="18">&nbsp;移除过滤器驱动程序...</div>

					
					<div id="deployAgentlessToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_deployAgentlessToolbarDropMenuItem_img" alt="" src="/jsj/deploy_appliance.svg" style="vertical-align:top;" width="18" height="18">&nbsp;部署无客户端安全...</div>

					
					<div id="unDeployAgentlessToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_unDeployAgentlessToolbarDropMenuItem_img" alt="" src="/jsj/delete.svg" style="vertical-align:top;" width="18" height="18">&nbsp;移除客户端</div>

					
					<div id="upgradeFastPathDriverToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_upgradeFastPathDriverToolbarDropMenuItem_img" alt="" src="/jsj/icon_upgrade_filter_driver.png" style="vertical-align:top;" width="18" height="18">&nbsp;升级过滤器驱动程序...</div>

					
					<div id="activateApplianceToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_activateApplianceToolbarDropMenuItem_img" alt="" src="/jsj/activate_appliance.svg" style="vertical-align:top;" width="18" height="18">&nbsp;激活设备...</div>

					
					<div id="activateNSXApplianceToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_activateNSXApplianceToolbarDropMenuItem_img" alt="" src="/jsj/activate_appliance.svg" style="vertical-align:top;" width="18" height="18">&nbsp;激活/重新激活</div>

					
					<div id="deactivateApplianceToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_deactivateApplianceToolbarDropMenuItem_img" alt="" src="/jsj/deactivate_appliance.svg" style="vertical-align:top;" width="18" height="18">&nbsp;停用设备</div>

					
					<div id="upgradeApplianceToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_upgradeApplianceToolbarDropMenuItem_img" alt="" src="/jsj/upgrade_appliance.svg" style="vertical-align:top;" width="18" height="18">&nbsp;升级设备...</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="updateNowToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_updateNowToolbarDropMenuItem_img" alt="" src="/jsj/send_policy.svg" style="vertical-align:top;" width="18" height="18">&nbsp;发送策略</div>

					
					<div id="cancelUpdateToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_cancelUpdateToolbarDropMenuItem_img" alt="" src="/jsj/cancel_send_policy.svg" style="vertical-align:top;" width="18" height="18">&nbsp;取消“发送策略”</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="componentUpdateToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_componentUpdateToolbarDropMenuItem_img" alt="" src="/jsj/update_components.svg" style="vertical-align:top;" width="18" height="18">&nbsp;下载安全更新</div>

					
					<div id="componentRollbackToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_componentRollbackToolbarDropMenuItem_img" alt="" src="/jsj/rollback_components.svg" style="vertical-align:top;" width="18" height="18">&nbsp;还原安全更新</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="getEventsNowToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_getEventsNowToolbarDropMenuItem_img" alt="" src="/jsj/get_events.svg" style="vertical-align:top;" width="18" height="18">&nbsp;获取事件</div>

					
					<div id="clearWarningsAndErrorsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_clearWarningsAndErrorsToolbarDropMenuItem_img" alt="" src="/jsj/clear_warning_error.svg" style="vertical-align:top;" width="18" height="18">&nbsp;清除警告/错误</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="upgradeAgentSoftwareToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_upgradeAgentSoftwareToolbarDropMenuItem_img" alt="" src="/jsj/upgrade_agent_software.svg" style="vertical-align:top;" width="18" height="18">&nbsp;升级客户端软件...</div>

					
					<div id="cancelUpgradeToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_cancelUpgradeToolbarDropMenuItem_img" alt="" src="/jsj/cancel_upgrade_agent_software.svg" style="vertical-align:top;" width="18" height="18">&nbsp;取消升级客户端软件</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="scanForRecommendationsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_scanForRecommendationsToolbarDropMenuItem_img" alt="" src="/jsj/scan_for_recommendations.svg" style="vertical-align:top;" width="18" height="18">&nbsp;漏洞扫描 (推荐设置)</div>

					
					<div id="cancelRecommendationScanToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_cancelRecommendationScanToolbarDropMenuItem_img" alt="" src="/jsj/cancel_scan_for_recommendation.svg" style="vertical-align:top;" width="18" height="18">&nbsp;取消“漏洞扫描 (推荐设置)”</div>

					
					<div id="clearRecommendationsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_clearRecommendationsToolbarDropMenuItem_img" alt="" src="/jsj/clear_recommendation.svg" style="vertical-align:top;" width="18" height="18">&nbsp;清除建议</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareQuickScanToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareQuickScanToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--quick_scan_for_malware.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;快速扫描恶意软件</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareManualScanToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareManualScanToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--full_scan_for_malware.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;完整扫描恶意软件</div>

					
					<div id="com_trendmicro_ds_antimalware_antiMalwareCancelScanToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_antimalware_antiMalwareCancelScanToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.antimalware-images-icons--abort_scan_for_malware.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;取消恶意软件扫描</div>

					
					<div id="com_trendmicro_ds_network_scanForOpenPortsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_network_scanForOpenPortsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--port_scan.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;扫描打开的端口</div>

					
					<div id="com_trendmicro_ds_network_cancelScanForOpenPortsToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_network_cancelScanForOpenPortsToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.network-images-icons--port_scan_cancel.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;取消扫描打开的端口</div>

					
					<div id="com_trendmicro_ds_integrity_scanForSystemIntegrityToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_integrity_scanForSystemIntegrityToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--scan_for_integrity.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;扫描完整性</div>

					
					<div id="com_trendmicro_ds_integrity_rebuildBaselineToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_integrity_rebuildBaselineToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--rebuild_computer_baseline.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;重新生成完整性基线</div>

					
					<div id="com_trendmicro_ds_integrity_scanPasswordToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_integrity_scanPasswordToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.integrity-images-icons--scanweakpassword.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;扫描弱口令</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlTurnOffMaintenanceModeToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_appcontrol_appControlTurnOffMaintenanceModeToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;关闭维护模式</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlTurnOnMaintenanceModeToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_appcontrol_appControlTurnOnMaintenanceModeToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;打开维护模式...</div>

					
					<div id="com_trendmicro_ds_appcontrol_appControlExtendMaintenanceModeToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_com_trendmicro_ds_appcontrol_appControlExtendMaintenanceModeToolbarDropMenuItem_img" alt="" src="/jsj/com.trendmicro.ds.appcontrol-images-icons--app_control_maintenance_mode.asvg.svg" style="vertical-align:top;" width="18" height="18">&nbsp;修改维护模式...</div>

					 <div class="toolbardropmenu_spacer"></div>
 
					<div id="moveToGroupToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_moveToGroupToolbarDropMenuItem_img" alt="" src="/jsj/move_to_group.svg" style="vertical-align:top;" width="18" height="18">&nbsp;移动到组...</div>

					
					<div id="assignSecurityProfileToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_assignSecurityProfileToolbarDropMenuItem_img" alt="" src="/jsj/assign_policy.svg" style="vertical-align:top;" width="18" height="18">&nbsp;分配策略...</div>

					
					<div id="assignHostAssetImportanceToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_assignHostAssetImportanceToolbarDropMenuItem_img" alt="" src="/jsj/assign_asset_value.svg" style="vertical-align:top;" width="18" height="18">&nbsp;分配资产值...</div>

					
					<div id="assignHostHistoryIAUToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_assignHostHistoryIAUToolbarDropMenuItem_img" alt="" src="/jsj/historyupdate.svg" style="vertical-align:top;" width="18" height="18">&nbsp;分配历史更新源...</div>

					
					<div id="assignRelayListToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_assignRelayListToolbarDropMenuItem_img" alt="" src="/jsj/assign_relay_group.svg" style="vertical-align:top;" width="18" height="18">&nbsp;分配中继组...</div>

					
		</div>


		<div id="toolbarDropMenuNew" class="toolbardropmenu_box" style="width:auto;position:absolute;display:none;top:65px;left:0px;z-index:100;">

			<div class="toolbardropmenu_item" tabindex="0"><img alt="" src="/jsj/servers.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加计算机...</div>

			
			
				<div id="syncWithDirectoriesMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_syncWithDirectoriesMenuItem_img" alt="" src="/jsj/ad_add_directory.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加 Active Directory...</div>

			
			
				<div id="syncWithVCenterMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_syncWithVCenterMenuItem_img" alt="" src="/jsj/vcenters_top.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加 Virtual Center...</div>

			
			
				
				<div id="syncWithAWSMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_syncWithAWSMenuItem_img" alt="" src="/jsj/aws_account.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加 AWS 帐户...</div>

				
				
				<div id="syncWithAzureMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_syncWithAzureMenuItem_img" alt="" src="/jsj/azure_top.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加 Azure 帐户...</div>

				
				
				<div id="syncWithVCloudMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_syncWithVCloudMenuItem_img" alt="" src="/jsj/vcloud_top.svg" style="vertical-align:top;" width="18" height="18">&nbsp;添加 vCloud 帐户...</div>

				
			
			
			<div class="toolbardropmenu_spacer"></div>
		
			
				<div class="toolbardropmenu_item" tabindex="0"><img alt="" src="/jsj/smart_folder.svg" style="vertical-align:top;" width="18" height="18">&nbsp;创建智能文件夹...</div>

			
			<div class="toolbardropmenu_item" tabindex="0"><img alt="" src="/jsj/folder.svg" style="vertical-align:top;" width="18" height="18">&nbsp;创建组...</div>

			
				<div class="toolbardropmenu_spacer"></div>

				<div id="discoverToolbarDropMenuItem" class="toolbardropmenu_item" tabindex="0"><img id="tbi_discoverToolbarDropMenuItem_img" alt="" src="/jsj/discover.svg" style="vertical-align:top;" width="18" height="18">&nbsp;查找...</div>

			
			<div class="toolbardropmenu_spacer"></div>

			<div class="toolbardropmenu_item" tabindex="0"><img alt="" src="/jsj/import.svg" style="vertical-align:top;" width="18" height="18">&nbsp;从文件导入...</div>

		</div>

		<div id="toolbarDropMenuExport" class="toolbardropmenu_box" style="width:320px;position:absolute;display:none;top:65px;left:365px;z-index:100;">

			<div id="toolbarDropMenuExportCsvDisplayed" class="toolbardropmenu_item" tabindex="0"><img id="tbi_toolbarDropMenuExportCsvDisplayed_img" alt="" src="/jsj/export.svg" style="vertical-align:top;" width="18" height="18">&nbsp;导出为 CSV...</div>

			<div id="toolbarDropMenuExportCsvSelected" class="toolbardropmenu_item" tabindex="0"><img id="tbi_toolbarDropMenuExportCsvSelected_img" alt="" src="/jsj/export.svg" style="vertical-align:top;" width="18" height="18">&nbsp;将选定内容导出为 CSV...</div>

			<div class="toolbardropmenu_spacer"></div>

			<div id="toolbarDropMenuExportXmlDisplayed" class="toolbardropmenu_item" tabindex="0"><img id="tbi_toolbarDropMenuExportXmlDisplayed_img" alt="" src="/jsj/export.svg" style="vertical-align:top;" width="18" height="18">&nbsp;导出为 XML (供导入)...</div>

			<div id="toolbarDropMenuExportXmlSelected" class="toolbardropmenu_item" tabindex="0"><img id="tbi_toolbarDropMenuExportXmlSelected_img" alt="" src="/jsj/export.svg" style="vertical-align:top;" width="18" height="18">&nbsp;将选定内容导出为 XML (供导入)...</div>

		</div>

		<form name="mainForm" method="post" action="https://10.209.135.108:4119/Hosts.screen" id="mainForm" class="datatable_container"><input id="rID" name="rID" type="hidden" value="EA3D9CFD338C05F6348BC5397C187D73"><input id="cmdArguments" name="cmdArguments" type="hidden" value=""><input id="command" name="command" type="hidden" value=""><input id="changed" name="changed" type="hidden" value="false">
			
			
			<!-- closing top section div -->
			<div id="mainSection" style="overflow: auto; height: 454px; width: 1336px;">
				<div id="mainTable" class="datatable">


					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
											
					
				<div id="mainTable_sizer" style="position:absolute;top:-1000px;left:0px;display:none;"></div><div id="mainTable_header" class="datatable_header_bk" style="top: 0px;">
<div><table id="mainTable_header_table" style="table-layout: fixed;">
<tbody>
<tr id="mainTable_headerRow"><td class="datatable_column" id="mainTable_c_0" style="width:16px;cursor:pointer;"><div class="datatable_overflow" style="width:16px;" id="mainTable_ti_0"><div class="datatable_text dt_center">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_1" style="width:146px;cursor:pointer;"><div class="datatable_overflow" style="width:146px;" id="mainTable_ti_1"><div class="datatable_text">名称&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td>

<td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_3" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_3"><div class="datatable_text">平台&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_4" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_4"><div class="datatable_text">策略&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_5" style="width:16px;cursor:pointer;"><div class="datatable_overflow" style="width:16px;" id="mainTable_ti_5"><div class="datatable_text dt_center">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_6" style="width:146px;cursor:pointer;"><div class="datatable_overflow" style="width:146px;" id="mainTable_ti_6"><div class="datatable_text">状态&nbsp;<img alt="" src="/jsj/sort_up.svg" class="datatable_sort_icon" width="12" height="12"></div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_7" style="width:146px;cursor:default;"><div class="datatable_overflow" style="width:146px;" id="mainTable_ti_7"><div class="datatable_text">任务&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_8" style="width:40px;cursor:pointer;"><div class="datatable_overflow" style="width:40px;" id="mainTable_ti_8"><div class="datatable_text dt_center"><img src="/jsj/com.trendmicro.ds.antimalware-images-icons--anti_malware.asvg.svg" title="防恶意软件状态指示灯" alt="防恶意软件状态指示灯" width="20px;" height="20px;">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_9" style="width:40px;cursor:pointer;"><div class="datatable_overflow" style="width:40px;" id="mainTable_ti_9"><div class="datatable_text dt_center"><img src="/jsj/com.trendmicro.ds.network-images-icons--dpi.asvg.svg" title="入侵防御状态指示灯" alt="入侵防御状态指示灯" width="20px;" height="20px;">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_10" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_10"><div class="datatable_text">维护模式&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_11" style="width:196px;cursor:pointer;"><div class="datatable_overflow" style="width:196px;" id="mainTable_ti_11"><div class="datatable_text">成功发送策略&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_13" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_12"><div class="datatable_text">恶意软件上次预设扫描&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="/jsj/blank.gif" width="3" height="1"></td><td style="width:100%"></td>
</tr>
</tbody>
</table>
</div></div>
							<div id="mainTable_rows" class="datatable_rows_bk">
								<div id="mainTable_rows_groupBar_0" style="width: 1414px;">
									<div id="mainTable_rows_groupBar_0_item"
										class="datatable_group_inner">
										<table id="mainTable_rows_groupBar_0_inner"
											style="width: 1404 px;">
											<tbody>
												<tr>
													<td><a href="javascript:" style="outline: 0;"><img
															id="mainTable_rows_groupBar_0_brancher" alt=""
															src="/jsj/tree_close.svg" class="tree-icon"
															style="padding-left: 6px; padding-top: 5px;" width="18"
															height="18"></a></td>
													<td id="mainTable_rows_groupBar_0_text1"
														class="datatable_group_text_2">&nbsp;</td>
													<td id="mainTable_rows_groupBar_0_text2"
														class="datatable_group_text">计算机<span
														class="datatable_group_count"> (2)</span></td>
													<td id="mainTable_rows_groupBar_0_text3"
														style="width: 99%; text-align: right;"
														class="datatable_group_text_2">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div id="mainTable_rows_groupBar_0_children"
									class="table-group-children" style="padding-left: 0px;">
									<table id="mainTable_rows_table_0" style="table-layout: fixed;">
										<tbody>
											<tr id="mainTable_row_1" tabindex="0"
												class="datatable_row datatable_row_selected">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="relay_server.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.135.108</div>
													</div></td>


												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Red Hat Enterprise 7 (64
															bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_1" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_1"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_2" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="relay_server.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.135.107</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Red Hat Enterprise 7 (64
															bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_2" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_2"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="mainTable_rows_groupBar_1" style="width: 1414px;">
									<div id="mainTable_rows_groupBar_1_item"
										class="datatable_group_inner">
										<table id="mainTable_rows_groupBar_1_inner"
											style="width: 1404 px;">
											<tbody>
												<tr>
													<td><a href="javascript:" style="outline: 0;"><img
															id="mainTable_rows_groupBar_1_brancher" alt=""
															src="/jsj/tree_close.svg" class="tree-icon"
															style="padding-left: 6px; padding-top: 5px;" width="18"
															height="18"></a></td>
													<td id="mainTable_rows_groupBar_1_text1"
														class="datatable_group_text_2">&nbsp;</td>
													<td id="mainTable_rows_groupBar_1_text2"
														class="datatable_group_text">计算机 &gt; Unix主机<span
														class="datatable_group_count"> (3)</span></td>
													<td id="mainTable_rows_groupBar_1_text3"
														style="width: 99%; text-align: right;"
														class="datatable_group_text_2">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div id="mainTable_rows_groupBar_1_children"
									class="table-group-children" style="padding-left: 0px;">
									<table id="mainTable_rows_table_1" style="table-layout: fixed;">
										<tbody>
											<tr id="mainTable_row_360" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.199.117</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">AIX 7.1 powerpc</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离全局配置的覆盖"
															style="font-weight: bold;">无</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_DD3127.svg"
																title="严重 [红色]" alt="严重 [红色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">脱机</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">更新配置未决 (脱机)</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/not_supported.svg"
																title="防恶意软件: 不支持" alt="防恶意软件: 不支持" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 分接" alt="入侵防御: 打开, 分接" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-06-09 21:18</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_360" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_360"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_361" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.199.118</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">AIX 7.1 powerpc</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/not_supported.svg"
																title="防恶意软件: 不支持" alt="防恶意软件: 不支持" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="入侵防御: 关闭" alt="入侵防御: 关闭" width="18" height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:13</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_361" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_361"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_359" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.204.51.76</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">AIX 6.1 powerpc</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/not_supported.svg"
																title="防恶意软件: 不支持" alt="防恶意软件: 不支持" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="入侵防御: 关闭" alt="入侵防御: 关闭" width="18" height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_359" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_359"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
										</tbody>
									</table>
								</div>


								<div id="mainTable_rows_groupBar_3" style="width: 1414px;">
									<div id="mainTable_rows_groupBar_3_item"
										class="datatable_group_inner">
										<table id="mainTable_rows_groupBar_3_inner"
											style="width: 1404 px;">
											<tbody>
												<tr>
													<td><a href="javascript:" style="outline: 0;"><img
															id="mainTable_rows_groupBar_3_brancher" alt=""
															src="/jsj/tree_close.svg" class="tree-icon"
															style="padding-left: 6px; padding-top: 5px;" width="18"
															height="18"></a></td>
													<td id="mainTable_rows_groupBar_3_text1"
														class="datatable_group_text_2">&nbsp;</td>
													<td id="mainTable_rows_groupBar_3_text2"
														class="datatable_group_text">计算机 &gt; 故障客户端<span
														class="datatable_group_count"> (7)</span></td>
													<td id="mainTable_rows_groupBar_3_text3"
														style="width: 99%; text-align: right;"
														class="datatable_group_text_2">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div id="mainTable_rows_groupBar_3_children"
									class="table-group-children" style="padding-left: 0px;">
									<table id="mainTable_rows_table_3" style="table-layout: fixed;">
										<tbody>
											<tr id="mainTable_row_305" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.231.251.20</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Microsoft Windows Server
															2003 (32 bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_DD3127.svg"
																title="严重 [红色]" alt="严重 [红色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">脱机</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">更新配置未决 (脱机)</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="入侵防御: 关闭, 未安装" alt="入侵防御: 关闭, 未安装" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-06-25 21:00</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">2021-04-23 10:14</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_305" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_305"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_77" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.211.70.30</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Microsoft Windows Server
															2008 (64 bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_DD3127.svg"
																title="严重 [红色]" alt="严重 [红色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">脱机</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">更新配置未决 (脱机)</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-04-07 08:36</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_77" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_77"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_113" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.231.251.22</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Microsoft Windows Server
															2003 (32 bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">2020-08-16 23:52</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_113" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_113"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_354" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.158.41</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Microsoft Windows Server
															2008 R2 (64 bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_354" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_354"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_375" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.204.54.116</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Red Hat Enterprise 7 (64
															bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">基本策略</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="入侵防御: 关闭, 未安装" alt="入侵防御: 关闭, 未安装" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:12</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_375" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_375"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_205" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.204.51.44</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">Microsoft Windows Server
															2008 R2 (64 bit)</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text" title="此计算机具有一个或多个偏离策略的覆盖"
															style="font-weight: bold;">Windows Server 2008</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="被管理 [绿色]" alt="被管理 [绿色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">被管理 (联机)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="防恶意软件: 打开, 实时" alt="防恶意软件: 打开, 实时" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_7CB842.svg"
																title="入侵防御: 打开, 阻止" alt="入侵防御: 打开, 阻止" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">2021-11-10 21:13</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">2020-10-16 00:46</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_205" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_205"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
											<tr id="mainTable_row_36" class="datatable_row">
												<td data-column="icon" style="width: 20px;"><div
														class="datatable_overflow" style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/servers.svg" alt=""
																width="18" height="18">
														</div>
													</div></td>
												<td class=" previewColumn" data-column="name"
													style="width: 150px;"><div class="datatable_overflow"
														style="width: 150px;">
														<div class="datatable_text">10.209.47.73</div>
													</div></td>
												
												<td data-column="summaryShortAgentReportedHostType"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">未知</div>
													</div></td>
												<td data-column="summarySecurityProfile"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">无</div>
													</div></td>
												<td data-column="summaryMergedHostLightIcon"
													style="width: 20px;"><div class="datatable_overflow"
														style="width: 20px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="未被管理 [灰色]" alt="未被管理 [灰色]" width="18" height="18">
														</div>
													</div></td>
												<td class="datatable_cell_sort"
													data-column="summaryMergedHostStatus" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">未被管理 (未知)</div>
													</div></td>
												<td data-column="summaryMergedTasks" style="width: 150px;"><div
														class="datatable_overflow" style="width: 150px;">
														<div class="datatable_text">&nbsp;</div>
													</div></td>
												<td data-column="module-AM-merged-icon" style="width: 44px;"><div
														class="datatable_overflow" style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="防恶意软件: 未激活" alt="防恶意软件: 未激活" width="18"
																height="18">
														</div>
													</div></td>
												<td data-column="module-DPI-merged-icon"
													style="width: 44px;"><div class="datatable_overflow"
														style="width: 44px;">
														<div class="dt_center">
															<img class="datatable_icon" src="/jsj/dot_D0D3D6.svg"
																title="入侵防御: 未激活" alt="入侵防御: 未激活" width="18" height="18">
														</div>
													</div></td>
												<td data-column="summaryAcMaintenanceModeStatus"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td data-column="summaryMergedLastSuccessfulUpdateSmart"
													style="width: 200px;"><div class="datatable_overflow"
														style="width: 200px;">
														<div class="datatable_text">N/A</div>
													</div></td>
												<td
													data-column="summaryMergedLastAMScheduledScanCompleteSmart"
													style="width: 100px;"><div class="datatable_overflow"
														style="width: 100px;">
														<div class="datatable_text">N/A</div>
													</div></td>
											</tr>
											<tr id="mainTable_row_hidden_36" class="datatable_row"
												style="display: none;">
												<td colspan="13" style="overflow: visible;"><div
														id="mainTable_row_hiddenHTML_36"
														style="white-space: nowrap; width: 1378px;"></div></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
<input type="hidden" id="mainTable_selected" name="mainTable_selected" value="1">
<input type="hidden" id="mainTable_selectedItems" name="mainTable_selectedItems" value="1">
<input type="hidden" id="mainTable_selectedGroup" name="mainTable_selectedGroup" value="">
<input type="hidden" id="mainTable_selectedGroups" name="mainTable_selectedGroups" value="">
<input type="hidden" id="mainTable_selectedGroupIdentifiers" name="mainTable_selectedGroupIdentifiers" value="">
<input type="hidden" id="mainTable_sortIndex" name="mainTable_sortIndex" value="6">
<input type="hidden" id="mainTable_sortColumnName" name="mainTable_sortColumnName" value="summaryMergedHostStatus">
<input type="hidden" id="mainTable_sortAsc" name="mainTable_sortAsc" value="true">
<input type="hidden" id="mainTable_viewstate" name="mainTable_viewstate" value="">
<input type="hidden" id="mainTable_groupByIndex" name="mainTable_groupByIndex" value="12">


			</div>	
			
</form>
			
			</div>
    	</div>
	</div>
	<div id="single-area" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: none;" class="spinner_large">
	</div>
	<div id="aboutModal" class="modal hide fade" style="width:875px;max-height:650px;margin:0pt 0pt 0pt -437px;">
		<div id="about_modal_header" class="modal-header">
			<a href="#" class="close">×</a>
			<h3>关于亚信安全服务器深度安全防护系统</h3>
		</div>
		<div id="about_modal_body" class="modal-body" style="max-height:none;">
			<div class="control_border" id="about">
				<div style="padding: 18px;">
					<table>
						<tbody>
							<tr>
								<td style="vertical-align: top;" width="60">
									<img src="/jsj/manager.svg" width="48" height="48">
								</td>
								<td height="60">
									<b>
									亚信安全服务器深度安全防护系统管理中心
									 
										(版本 10.5.5479)
									
									<br>
									版权所有 © 2017 亚信科技（成都）有限公司。保留所有权利
									</b>
									<br><br><br>
									Microsoft、Windows、Windows Vista、Windows Server、SQL Server 和 Active Directory 是 Microsoft Corporation 的注册商标。<br><br>Novell 和 eDirectory 是 Novell, Inc. 的注册商标。<br><br>Oracle、Solaris 和 Java 是 Oracle 和/或其附属公司的注册商标。<br><br>VMware、VMware vSphere、ESXi Server 和“框”徽标及设计是 VMware, Inc. 的注册商标。<br><br>Red Hat 是 Red Hat, Inc. 的注册商标。<br><br>SUSE 是 Novell, Inc. 的注册商标。<br><br>Linux 是 Linus Torvalds 的注册商标。<br><br>此处提及的所有其他标记和名称可能为各自公司的商标。
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	







	
	<!-- Navigation JS End -->


	<div class="formFooter" style="display:none">
</div>

  
<div id="status_bar_container"><div id="status_bar" class="sb_left"><div class="status_bar_body"><div class="status_bar_content"><div><div id="sb_tasks" style="width: 1258.4px;"><div id="sb_tasks_area" class="sb_tasks_area" style="width: 1736px; left: 0px;"></div></div><div class="status_bar_item" id="sb_alerts"><span class="sb_alerts_title">警报</span><div class="sb_alert" id="sb_alert_warnings" style="border: 2px solid #eeb81c;" title="警告">233</div><div class="sb_alert" id="sb_alert_critical" style="border: 2px solid #DD3127;" title="严重">4</div></div></div></div></div></div><div id="sb_tasks_scroll" class="sb_tasks_scroll" style="left: 1320.4px;"><span id="sb_scroll_right" class="sb_scroll sb_scroll_right sb_scroll_disabled" style="display: none;"></span><span id="sb_scroll_left" class="sb_scroll sb_scroll_left sb_scroll_disabled" style="display: none;"></span></div></div>
</body></html>