<%@ page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html><html data-scrapbook-source="https://10.209.135.108:4119/Administrators.screen?setHash=users"><head><meta charset="UTF-8">
		<title>用户</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
		<script src="lib/bootstrap/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="bootstrap-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="styles-zh_CN-1065112.csa.css">

		




		

		
	</head>
	<body>


		<form name="mainForm" method="post" action="https://10.209.135.108:4119/Administrators.screen" id="mainForm" class=""><input id="rID" name="rID" type="hidden" value="5469B2950DF78CFDC531B2E1B46A2E63"><input id="cmdArguments" name="cmdArguments" type="hidden" value=""><input id="command" name="command" type="hidden" value=""><input id="changed" name="changed" type="hidden" value="false">
			<input id="searchOpen" name="searchOpen" type="hidden" value="">
			<input id="scrollTop" name="scrollTop" type="hidden" value="">
			<input id="scrollLeft" name="scrollLeft" type="hidden" value="">
			<div id="topSection">
				<div id="mainMessage" class="alert-message" style="display:none;height:1px;overflow:hidden;"><table style="margin-bottom:0px;"><tbody><tr><td style="width:24px"><img id="mainMessage_icon" src="blank.gif" style="vertical-align:middle" alt="" width="24" height="24"> </td><td style="padding-left: 10px;"><span id="mainMessage_text"></span></td><td style="width:24px"><a id="mainMessage_close" class="close" href="#">×</a></td></tr></tbody></table></div><div id="mainMessage_ghost" class="alert-message" style="position:absolute;top:-1000px;left:-1000px;z-index:999997;"></div> 

				<div class="main_header">
				<table><tbody><tr>
				<td style="white-space:nowrap;width:1%;padding-right:10px"><span class="main_header_text">用户</span></td>
				
				
				</tr></tbody>
				
				</table>
				
				</div>				
				<div id="search" style="display:none">
					<table class="searchContainer">
						<tbody>
							<tr>
								<td>
								
									
									<div id="searchCriteriaDiv_0" style="">
										<div class="search_toolbar">

											<div id="searchBarSize_0" style="overflow:hidden;">
												<input id="searchActive_0" name="searchActive_0" type="text" value="true" style="display:none;">
												<table><tbody><tr>
													<td style="width:82px;">
														<div style="width:82px;" class="align-right">搜索:</div>
													</td>

													<td>
														<select id="searchColumn_0" name="searchColumn_0" style="width:250px;" size="1">
															<option value="username" title="用户名" selected="selected">用户名</option>
															<option value="name" title="名称">名称</option>
															
															<option value="role" title="角色">角色</option>
															
															
															
															
														</select>

													</td>

													<td>
														<select id="searchType_0" name="searchType_0" style="width:150px;" size="1">
															<option value="LIKE" title="包含" selected="selected">包含</option>
															<option value="NOT LIKE" title="不包含">不包含</option>
															<option value="EQUAL" title="等于">等于</option>
															<option value="NOT EQUAL" title="不等于">不等于</option>
															<option value="IN" title="在范围内">在范围内</option>
															<option value="NOT IN" title="不在范围内">不在范围内</option>
														</select>

													</td>

													<td>
														<input id="searchQuery_0" name="searchQuery_0" type="text" value="" class="searchCriteria" maxlength="254">
													</td>

												</tr></tbody></table>

											</div>
										</div>

									</div>
								
								
								</td>
								













<!-- Default values for inputs -->


 <td class="no-bootstrap-filter-submit"> 
<button id="filter-submit-button" tabindex="0" class="filter-submit-btn refresh-btn" title="搜索">
	<img src="refresh_search_dark.svg" style="vertical-align:middle" class="img-medium icon-disabled" alt="Update Filters">
	<img src="refresh_search.svg" style="vertical-align:middle" class="img-medium icon-active" alt="Update Filters">
	
</button>
 </td> 


							</tr>
						</tbody>
					</table>
				</div>


			</div> <!-- closing top section div -->
			<div id="mainSection" style="overflow: auto; height: 487px; width: 1336px;">
				<div id="mainTable" class="datatable">


					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				<div id="mainTable_sizer" style="position:absolute;top:-1000px;left:0px;display:none;"></div><div id="mainTable_header" class="datatable_header_bk" style="top: 0px;">
<div><table id="mainTable_header_table" style="table-layout: fixed;">
<tbody>
<tr id="mainTable_headerRow"><td class="datatable_column" id="mainTable_c_0" style="width:16px;cursor:pointer;"><div class="datatable_overflow" style="width:16px;" id="mainTable_ti_0"><div class="datatable_text dt_center">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_1" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_1"><div class="datatable_text">用户名&nbsp;<img alt="" src="sort_up.svg" class="datatable_sort_icon" width="12" height="12"></div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_2" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_2"><div class="datatable_text">名称&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_4" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_3"><div class="datatable_text dt_center">已锁定&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_5" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_4"><div class="datatable_text dt_center">已登录&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_6" style="width:156px;cursor:pointer;"><div class="datatable_overflow" style="width:156px;" id="mainTable_ti_5"><div class="datatable_text dt_center">最后一次登录&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_7" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_6"><div class="datatable_text dt_center">MFA&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_8" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_7"><div class="datatable_text dt_center">密码过期&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td style="width:100%"></td>
</tr>
</tbody>
</table>
</div></div>
				<div id="mainTable_rows" class="datatable_rows_bk">
					<div id="mainTable_rows_groupBar_0" style="width: 736px;">
						<div id="mainTable_rows_groupBar_0_item"
							class="datatable_group_inner">
							<table id="mainTable_rows_groupBar_0_inner"
								style="width: 726 px;">
								<tbody>
									<tr>
										<td><a href="javascript:" style="outline: 0;"><img
												id="mainTable_rows_groupBar_0_brancher" alt=""
												src="tree_close.svg" class="tree-icon"
												style="padding-left: 6px; padding-top: 5px;" width="18"
												height="18"></a></td>
										<td id="mainTable_rows_groupBar_0_text1"
											class="datatable_group_text_2">&nbsp;</td>
										<td id="mainTable_rows_groupBar_0_text2"
											class="datatable_group_text">安全访问权限<span
											class="datatable_group_count"> (1)</span></td>
										<td id="mainTable_rows_groupBar_0_text3"
											style="width: 99%; text-align: right;"
											class="datatable_group_text_2">&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="mainTable_rows_groupBar_0_children"
						class="table-group-children" style="padding-left: 0px;">
						<table id="mainTable_rows_table_0" style="table-layout: fixed;">
							<tbody>
								<tr id="mainTable_row_6" tabindex="0"
									class="datatable_row datatable_row_selected">
									<td data-column="icon" style="width: 20px;"><div
											class="datatable_overflow" style="width: 20px;">
											<div class="dt_center">
												<img class="datatable_icon" src="user.svg" alt="" width="18"
													height="18">
											</div>
										</div></td>
									<td class="datatable_cell_sort" data-column="username"
										style="width: 100px;"><div class="datatable_overflow"
											style="width: 100px;">
											<div class="datatable_text">admin</div>
										</div></td>
									<td data-column="name" style="width: 100px;"><div
											class="datatable_overflow" style="width: 100px;">
											<div class="datatable_text">&nbsp;</div>
										</div></td>
									<td data-column="lockoutIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="signedInIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="summaryLastSignIn" style="width: 160px;"><div
											class="datatable_overflow" style="width: 160px;">
											<div class="datatable_text dt_center"></div>
										</div></td>
									<td data-column="mfaEnabledIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="passwordExpiredIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
								</tr>
							</tbody>
						</table>
					</div>


<div id="mainTable_rows_groupBar_1" style="width: 736px;">
						<div id="mainTable_rows_groupBar_0_item"
							class="datatable_group_inner">
							<table id="mainTable_rows_groupBar_0_inner"
								style="width: 726 px;">
								<tbody>
									<tr>
										<td><a href="javascript:" style="outline: 0;"><img
												id="mainTable_rows_groupBar_0_brancher" alt=""
												src="tree_close.svg" class="tree-icon"
												style="padding-left: 6px; padding-top: 5px;" width="18"
												height="18"></a></td>
										<td id="mainTable_rows_groupBar_0_text1"
											class="datatable_group_text_2">&nbsp;</td>
										<td id="mainTable_rows_groupBar_0_text2"
											class="datatable_group_text">test<span
											class="datatable_group_count"> (1)</span></td>
										<td id="mainTable_rows_groupBar_0_text3"
											style="width: 99%; text-align: right;"
											class="datatable_group_text_2">&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="mainTable_rows_groupBar_1_children"
						class="table-group-children" style="padding-left: 0px;">
						<table id="mainTable_rows_table_0" style="table-layout: fixed;">
							<tbody>
								<tr id="mainTable_row_6" tabindex="0"
									class="datatable_row datatable_row_selected">
									<td data-column="icon" style="width: 20px;"><div
											class="datatable_overflow" style="width: 20px;">
											<div class="dt_center">
												<img class="datatable_icon" src="user.svg" alt="" width="18"
													height="18">
											</div>
										</div></td>
									<td class="datatable_cell_sort" data-column="username"
										style="width: 100px;"><div class="datatable_overflow"
											style="width: 100px;">
											<div class="datatable_text">test</div>
										</div></td>
									<td data-column="name" style="width: 100px;"><div
											class="datatable_overflow" style="width: 100px;">
											<div class="datatable_text">&nbsp;</div>
										</div></td>
									<td data-column="lockoutIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="signedInIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="summaryLastSignIn" style="width: 160px;"><div
											class="datatable_overflow" style="width: 160px;">
											<div class="datatable_text dt_center"></div>
										</div></td>
									<td data-column="mfaEnabledIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="passwordExpiredIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
<input type="hidden" id="mainTable_selected" name="mainTable_selected" value="6">
<input type="hidden" id="mainTable_selectedItems" name="mainTable_selectedItems" value="6">
<input type="hidden" id="mainTable_selectedGroup" name="mainTable_selectedGroup" value="">
<input type="hidden" id="mainTable_selectedGroups" name="mainTable_selectedGroups" value="">
<input type="hidden" id="mainTable_selectedGroupIdentifiers" name="mainTable_selectedGroupIdentifiers" value="">
<input type="hidden" id="mainTable_sortIndex" name="mainTable_sortIndex" value="1">
<input type="hidden" id="mainTable_sortColumnName" name="mainTable_sortColumnName" value="username">
<input type="hidden" id="mainTable_sortAsc" name="mainTable_sortAsc" value="true">
<input type="hidden" id="mainTable_viewstate" name="mainTable_viewstate" value="">
<input type="hidden" id="mainTable_groupByIndex" name="mainTable_groupByIndex" value="3">


				
		
			</div>			
			
		<div class="formFooter" style="display:none">
</div>
</form>

	

</body></html>