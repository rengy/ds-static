<%@ page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html><html data-scrapbook-source="https://10.209.135.108:4119/Application.screen?#administration_users" data-scrapbook-create="20211120131112909" data-scrapbook-title="亚信安全服务器深度安全防护系统管理中心" data-scrapbook-type="site"><head><meta charset="UTF-8">
	<title>亚信安全服务器深度安全防护系统管理中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
		<script src="lib/bootstrap/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="/gl/bootstrap-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/tmStatusBar-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/splitter-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/tmDropDownList-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/tmAccordion-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/tmNavigator-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/gl/styles-zh_CN-1065112.csa.css">

	
  </head>

  <body style="overflow: hidden; background-color: #ddd;">
	

<div id="tm_header" class="unselectable cfx">
	<a id="tm_logo_link"><img class="tm_logo" src="/gl/product_logo_zh_CN.svg"></a>

	

		<ul id="tm_header_links" data-dropdown="dropdown">

		
		<li class="dropdown">
			<span href="javascript:" class="">
				<strong id="signedInUsername">${username}</strong>

			</span>
			
		</li>
		<li>
			<a style="cursor: pointer;color: #fff;" href="/logout">
				注销
			</a>
		</li>
		</ul>

	

	<div id="nav_div" class="unselectable wrapped"><nav>
	<ul id="tm_navigator_ul"><li id="tab_dashboard" class="tab">
	<a class="tab_link" href="/admin">控制台</a></li>
	
	<!-- <li id="tab_alerts" class="tab"><a class="tab_link" href="javascript:">警报</a></li>
	<li id="tab_eventsAndReports" class="tab"><a class="tab_link" href="javascript:">事件和报告</a></li> -->
	
	<li id="tab_computers" class="tab">
	<a class="tab_link" href="/admin/jsj">计算机</a></li>
	
	<!-- <li id="tab_policies" class="tab"><a class="tab_link" href="javascript:">策略</a></li><li id="tab_edrview" class="tab"><a class="tab_link" href="javascript:">检测响应</a></li><li id="tab_asmview" class="tab"><a class="tab_link" href="javascript:">资产发现</a></li><li id="tab_hostreinforcement" class="tab"><a class="tab_link" href="javascript:">主机加固</a></li>
	 -->
	
	<li id="tab_administration" class="tab selected"><a class="tab_link" href="javascript:">管理</a></li>
	</ul></nav></div>

</div>




	<!--  insert any alert bars -->
	<div id="alert-bar">
		
		
		
		
		
		
	</div>	
	
 	<div id="split-area" class="container_splitter" style="">
    	<div id="splitter" class="splitter s_cfx" style="height: 588.4px;">
			<div class="sidebar" style="width: 190px;">
	            <div class="contentLeft spinner_large">
	                <div id="contextMenu" class="contextmenu_box" style="min-width:170px;position:absolute;display:none;z-index:100;">

        </div>


        <form name="mainForm" method="post" action="https://10.209.135.108:4119/AdministrationMenu.screen" id="mainForm" class=""><input id="rID" name="rID" type="hidden" value="5469B2950DF78CFDC531B2E1B46A2E63"><input id="cmdArguments" name="cmdArguments" type="hidden" value=""><input id="command" name="command" type="hidden" value=""><input id="changed" name="changed" type="hidden" value="false">
			<div id="mainMessage" class="alert-message" style="display:none;height:1px;overflow:hidden;"><table style="margin-bottom:0px;"><tbody><tr><td style="width:24px"><img id="mainMessage_icon" src="/gl/blank.gif" style="vertical-align:middle" alt="" width="24" height="24"> </td><td style="padding-left: 10px;"><span id="mainMessage_text"></span></td><td style="width:24px"><a id="mainMessage_close" class="close" href="#">×</a></td></tr></tbody></table></div><div id="mainMessage_ghost" class="alert-message" style="position:absolute;top:-1000px;left:-1000px;z-index:999997;"></div> 

			<div>
	            <div id="menu">
<div>
<div id="menu_systemSettings" class="menutreeview_item_row"><div id="menu_systemSettings_div" class="menutreeview_item" style="padding-left:26px;"><span id="menu_systemSettings_text" tabindex="0" class="menutreeview_item_text"><img src="/gl/settings.svg" alt="" width="18" height="18"><span>系统设置</span></span></div></div>









</div>
</div>
<input type="hidden" id="menu_viewstate" name="menu_viewstate" value="updates,software,rolesUsersContacts">
<input type="hidden" id="menu_selected" name="menu_selected" value="users">


	        </div>
        <div class="formFooter" style="display:none">
</div>
</form>

    

	            </div>
			</div><div class="splitbarV" unselectable="on" style="cursor: e-resize; user-select: none;"><span></span><div style="cursor: pointer;" class="splitbuttonV" unselectable="on"></div></div>
			<div class="content spinner_large" style="width: 1336px;"> <!-- The content frame was default with a spinner -->
				<form name="mainForm" method="post" action="https://10.209.135.108:4119/Administrators.screen" id="mainForm" class=""><input id="rID" name="rID" type="hidden" value="5469B2950DF78CFDC531B2E1B46A2E63"><input id="cmdArguments" name="cmdArguments" type="hidden" value=""><input id="command" name="command" type="hidden" value=""><input id="changed" name="changed" type="hidden" value="false">
			<input id="searchOpen" name="searchOpen" type="hidden" value="">
			<input id="scrollTop" name="scrollTop" type="hidden" value="">
			<input id="scrollLeft" name="scrollLeft" type="hidden" value="">
			<div id="topSection">
				<div id="mainMessage" class="alert-message" style="display:none;height:1px;overflow:hidden;"><table style="margin-bottom:0px;"><tbody><tr><td style="width:24px"><img id="mainMessage_icon" src="blank.gif" style="vertical-align:middle" alt="" width="24" height="24"> </td><td style="padding-left: 10px;"><span id="mainMessage_text"></span></td><td style="width:24px"><a id="mainMessage_close" class="close" href="#">×</a></td></tr></tbody></table></div><div id="mainMessage_ghost" class="alert-message" style="position:absolute;top:-1000px;left:-1000px;z-index:999997;"></div> 

				<div class="main_header">
				<table><tbody><tr>
				<td style="white-space:nowrap;width:1%;padding-right:10px"><span class="main_header_text">用户</span></td>
				
				
				</tr></tbody>
				
				</table>
				
				</div>				
				<div id="search" style="display:none">
					<table class="searchContainer">
						<tbody>
							<tr>
								<td>
								
									
									<div id="searchCriteriaDiv_0" style="">
										<div class="search_toolbar">

											<div id="searchBarSize_0" style="overflow:hidden;">
												<input id="searchActive_0" name="searchActive_0" type="text" value="true" style="display:none;">
												<table><tbody><tr>
													<td style="width:82px;">
														<div style="width:82px;" class="align-right">搜索:</div>
													</td>

													<td>
														<select id="searchColumn_0" name="searchColumn_0" style="width:250px;" size="1">
															<option value="username" title="用户名" selected="selected">用户名</option>
															<option value="name" title="名称">名称</option>
															
															<option value="role" title="角色">角色</option>
															
															
															
															
														</select>

													</td>

													<td>
														<select id="searchType_0" name="searchType_0" style="width:150px;" size="1">
															<option value="LIKE" title="包含" selected="selected">包含</option>
															<option value="NOT LIKE" title="不包含">不包含</option>
															<option value="EQUAL" title="等于">等于</option>
															<option value="NOT EQUAL" title="不等于">不等于</option>
															<option value="IN" title="在范围内">在范围内</option>
															<option value="NOT IN" title="不在范围内">不在范围内</option>
														</select>

													</td>

													<td>
														<input id="searchQuery_0" name="searchQuery_0" type="text" value="" class="searchCriteria" maxlength="254">
													</td>

												</tr></tbody></table>

											</div>
										</div>

									</div>
								
								
								</td>
								













<!-- Default values for inputs -->


 <td class="no-bootstrap-filter-submit"> 
<button id="filter-submit-button" tabindex="0" class="filter-submit-btn refresh-btn" title="搜索">
	<img src="refresh_search_dark.svg" style="vertical-align:middle" class="img-medium icon-disabled" alt="Update Filters">
	<img src="refresh_search.svg" style="vertical-align:middle" class="img-medium icon-active" alt="Update Filters">
	
</button>
 </td> 


							</tr>
						</tbody>
					</table>
				</div>


			</div> <!-- closing top section div -->
			<div id="mainSection" style="overflow: auto; height: 487px; width: 1336px;">
				<div id="mainTable" class="datatable">


					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				<div id="mainTable_sizer" style="position:absolute;top:-1000px;left:0px;display:none;"></div><div id="mainTable_header" class="datatable_header_bk" style="top: 0px;">
<div><table id="mainTable_header_table" style="table-layout: fixed;">
<tbody>
<tr id="mainTable_headerRow"><td class="datatable_column" id="mainTable_c_0" style="width:16px;cursor:pointer;"><div class="datatable_overflow" style="width:16px;" id="mainTable_ti_0"><div class="datatable_text dt_center">&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_1" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_1"><div class="datatable_text">用户名&nbsp;<img alt="" src="sort_up.svg" class="datatable_sort_icon" width="12" height="12"></div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_2" style="width:96px;cursor:pointer;"><div class="datatable_overflow" style="width:96px;" id="mainTable_ti_2"><div class="datatable_text">名称&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_4" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_3"><div class="datatable_text dt_center">已锁定&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_5" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_4"><div class="datatable_text dt_center">已登录&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_6" style="width:156px;cursor:pointer;"><div class="datatable_overflow" style="width:156px;" id="mainTable_ti_5"><div class="datatable_text dt_center">最后一次登录&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_7" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_6"><div class="datatable_text dt_center">MFA&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td class="datatable_column" id="mainTable_c_8" style="width:76px;cursor:pointer;"><div class="datatable_overflow" style="width:76px;" id="mainTable_ti_7"><div class="datatable_text dt_center">密码过期&nbsp;&nbsp;</div></div></td><td class="datatable_resizer" style="width:3px;"><img alt="" src="blank.gif" width="3" height="1"></td><td style="width:100%"></td>
</tr>
</tbody>
</table>
</div></div>
				<div id="mainTable_rows" class="datatable_rows_bk">
					<div id="mainTable_rows_groupBar_0" style="width: 736px;">
						<div id="mainTable_rows_groupBar_0_item"
							class="datatable_group_inner">
							<table id="mainTable_rows_groupBar_0_inner"
								style="width: 726 px;">
								<tbody>
									<tr>
										<td><a href="javascript:" style="outline: 0;"><img
												id="mainTable_rows_groupBar_0_brancher" alt=""
												src="/gl/tree_close.svg" class="tree-icon"
												style="padding-left: 6px; padding-top: 5px;" width="18"
												height="18"></a></td>
										<td id="mainTable_rows_groupBar_0_text1"
											class="datatable_group_text_2">&nbsp;</td>
										<td id="mainTable_rows_groupBar_0_text2"
											class="datatable_group_text">安全访问权限<span
											class="datatable_group_count"> (1)</span></td>
										<td id="mainTable_rows_groupBar_0_text3"
											style="width: 99%; text-align: right;"
											class="datatable_group_text_2">&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="mainTable_rows_groupBar_0_children"
						class="table-group-children" style="padding-left: 0px;">
						<table id="mainTable_rows_table_0" style="table-layout: fixed;">
							<tbody>
								<tr id="mainTable_row_6" tabindex="0"
									class="datatable_row datatable_row_selected">
									<td data-column="icon" style="width: 20px;"><div
											class="datatable_overflow" style="width: 20px;">
											<div class="dt_center">
												<img class="datatable_icon" src="user.svg" alt="" width="18"
													height="18">
											</div>
										</div></td>
									<td class="datatable_cell_sort" data-column="username"
										style="width: 100px;"><div class="datatable_overflow"
											style="width: 100px;">
											<div class="datatable_text">admin</div>
										</div></td>
									<td data-column="name" style="width: 100px;"><div
											class="datatable_overflow" style="width: 100px;">
											<div class="datatable_text">&nbsp;</div>
										</div></td>
									<td data-column="lockoutIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="signedInIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="summaryLastSignIn" style="width: 160px;"><div
											class="datatable_overflow" style="width: 160px;">
											<div class="datatable_text dt_center"></div>
										</div></td>
									<td data-column="mfaEnabledIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="passwordExpiredIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
								</tr>
							</tbody>
						</table>
					</div>


<div id="mainTable_rows_groupBar_1" style="width: 736px;">
						<div id="mainTable_rows_groupBar_0_item"
							class="datatable_group_inner">
							<table id="mainTable_rows_groupBar_0_inner"
								style="width: 726 px;">
								<tbody>
									<tr>
										<td><a href="javascript:" style="outline: 0;"><img
												id="mainTable_rows_groupBar_0_brancher" alt=""
												src="/gl/tree_close.svg" class="tree-icon"
												style="padding-left: 6px; padding-top: 5px;" width="18"
												height="18"></a></td>
										<td id="mainTable_rows_groupBar_0_text1"
											class="datatable_group_text_2">&nbsp;</td>
										<td id="mainTable_rows_groupBar_0_text2"
											class="datatable_group_text">test<span
											class="datatable_group_count"> (1)</span></td>
										<td id="mainTable_rows_groupBar_0_text3"
											style="width: 99%; text-align: right;"
											class="datatable_group_text_2">&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="mainTable_rows_groupBar_1_children"
						class="table-group-children" style="padding-left: 0px;">
						<table id="mainTable_rows_table_0" style="table-layout: fixed;">
							<tbody>
								<tr id="mainTable_row_6" tabindex="0"
									class="datatable_row datatable_row_selected">
									<td data-column="icon" style="width: 20px;"><div
											class="datatable_overflow" style="width: 20px;">
											<div class="dt_center">
												<img class="datatable_icon" src="user.svg" alt="" width="18"
													height="18">
											</div>
										</div></td>
									<td class="datatable_cell_sort" data-column="username"
										style="width: 100px;"><div class="datatable_overflow"
											style="width: 100px;">
											<div class="datatable_text">test</div>
										</div></td>
									<td data-column="name" style="width: 100px;"><div
											class="datatable_overflow" style="width: 100px;">
											<div class="datatable_text">&nbsp;</div>
										</div></td>
									<td data-column="lockoutIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="signedInIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="summaryLastSignIn" style="width: 160px;"><div
											class="datatable_overflow" style="width: 160px;">
											<div class="datatable_text dt_center"></div>
										</div></td>
									<td data-column="mfaEnabledIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
									<td data-column="passwordExpiredIcon" style="width: 80px;"><div
											class="datatable_overflow" style="width: 80px;">
											<div class="dt_center">&nbsp;</div>
										</div></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
<input type="hidden" id="mainTable_selected" name="mainTable_selected" value="6">
<input type="hidden" id="mainTable_selectedItems" name="mainTable_selectedItems" value="6">
<input type="hidden" id="mainTable_selectedGroup" name="mainTable_selectedGroup" value="">
<input type="hidden" id="mainTable_selectedGroups" name="mainTable_selectedGroups" value="">
<input type="hidden" id="mainTable_selectedGroupIdentifiers" name="mainTable_selectedGroupIdentifiers" value="">
<input type="hidden" id="mainTable_sortIndex" name="mainTable_sortIndex" value="1">
<input type="hidden" id="mainTable_sortColumnName" name="mainTable_sortColumnName" value="username">
<input type="hidden" id="mainTable_sortAsc" name="mainTable_sortAsc" value="true">
<input type="hidden" id="mainTable_viewstate" name="mainTable_viewstate" value="">
<input type="hidden" id="mainTable_groupByIndex" name="mainTable_groupByIndex" value="3">


				
		
			</div>			
			
		<div class="formFooter" style="display:none">
</div>
</form>
				
			</div>
    	</div>
	</div>
	<div id="single-area" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: none;" class="spinner_large">
	</div>
	<div id="aboutModal" class="modal hide fade" style="width:875px;max-height:650px;margin:0pt 0pt 0pt -437px;">
		<div id="about_modal_header" class="modal-header">
			<a href="#" class="close">×</a>
			<h3>关于亚信安全服务器深度安全防护系统</h3>
		</div>
		<div id="about_modal_body" class="modal-body" style="max-height:none;">
			<div class="control_border" id="about">
				<div style="padding: 18px;">
					<table>
						<tbody>
							<tr>
								<td style="vertical-align: top;" width="60">
									<img src="/gl/manager.svg" width="48" height="48">
								</td>
								<td height="60">
									<b>
									亚信安全服务器深度安全防护系统管理中心
									 
										(版本 10.5.5479)
									
									<br>
									版权所有 © 2017 亚信科技（成都）有限公司。保留所有权利
									</b>
									<br><br><br>
									Microsoft、Windows、Windows Vista、Windows Server、SQL Server 和 Active Directory 是 Microsoft Corporation 的注册商标。<br><br>Novell 和 eDirectory 是 Novell, Inc. 的注册商标。<br><br>Oracle、Solaris 和 Java 是 Oracle 和/或其附属公司的注册商标。<br><br>VMware、VMware vSphere、ESXi Server 和“框”徽标及设计是 VMware, Inc. 的注册商标。<br><br>Red Hat 是 Red Hat, Inc. 的注册商标。<br><br>SUSE 是 Novell, Inc. 的注册商标。<br><br>Linux 是 Linus Torvalds 的注册商标。<br><br>此处提及的所有其他标记和名称可能为各自公司的商标。
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	








  
<div id="status_bar_container"><div id="status_bar" class="sb_left"><div class="status_bar_body"><div class="status_bar_content"><div><div id="sb_tasks" style="width: 1258.4px;"><div id="sb_tasks_area" class="sb_tasks_area" style="width: 1736px; left: 0px;"><div class="sb_task" id="sb_task_0" title="正在 1 台计算机上执行安全更新"><div class="sb_text"><span class="multi-spinner"><div class="outer"><div class="middle"><div class="inner"></div></div></div></span><span id="sb_task_text_0" class="multi-spinner-text">正在 1 台计算机上执行安全更新</span></div></div></div></div><div class="status_bar_item" id="sb_alerts"><span class="sb_alerts_title">警报</span><div class="sb_alert" id="sb_alert_warnings" style="border: 2px solid #eeb81c;" title="警告">232</div><div class="sb_alert" id="sb_alert_critical" style="border: 2px solid #DD3127;" title="严重">6</div></div></div></div></div></div><div id="sb_tasks_scroll" class="sb_tasks_scroll" style="left: 1320.4px;"><span id="sb_scroll_right" class="sb_scroll sb_scroll_right sb_scroll_disabled" style="display: none;"></span><span id="sb_scroll_left" class="sb_scroll sb_scroll_left sb_scroll_disabled" style="display: none;"></span></div></div></body></html>