<%@ page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html><html data-scrapbook-source="https://10.209.135.108:4119/Application.screen?#dashboard" data-scrapbook-create="20211119134235423" data-scrapbook-title="亚信安全服务器深度安全防护系统管理中心" data-scrapbook-type="site"><head><meta charset="UTF-8">
	<title>亚信安全服务器深度安全防护系统管理中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
		<script src="lib/bootstrap/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="/kzht/bootstrap-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/tmStatusBar-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/splitter-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/tmDropDownList-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/tmAccordion-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/tmNavigator-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/kzht/styles-zh_CN-1065112.csa.css">

	
	




		
		
		
		
		
		
		
		
		
		

  </head>

  <body style="overflow: hidden; background-color: #ddd;">
	


























<div id="tm_header" class="unselectable cfx">
	<a id="tm_logo_link"><img class="tm_logo" src="/kzht/product_logo_zh_CN.svg"></a>

	

		<ul id="tm_header_links" data-dropdown="dropdown">

		
		<li class="dropdown">
			<span href="javascript:" class="">
				<strong id="signedInUsername">${username}</strong>

			</span>
			
		</li>
		<li>
			<a style="cursor: pointer;color: #fff;" href="/logout">
				注销
			</a>
		</li>
		</ul>

	

	<div id="nav_div" class="unselectable wrapped"><nav><ul id="tm_navigator_ul">
	<li id="tab_dashboard" class="tab selected"><a class="tab_link" href="javascript:">控制台</a></li>
	<li id="tab_computers" class="tab">
	<a class="tab_link" href="/admin/jsj">计算机</a></li>
	<% String username=(String)request.getAttribute("username");if(username.equals("admin")){%>	
					<li id="tab_administration" class="tab"><a class="tab_link"
						href="/admin/gl">管理</a></li>
					<% }%>	
	
	</ul></nav></div>
</div>




	<!--  insert any alert bars -->
	<div id="alert-bar">
		
		
		
		
		
		
	</div>	
	
 	<div id="split-area" class="container_splitter" style="display: none;">
    	<div id="splitter" class="splitter s_cfx" style="height: 588.4px;">
			<div class="sidebar" style="width: 190px;">
	            <div class="contentLeft spinner_large">
	            </div>
			</div><div class="splitbarV" unselectable="on" style="cursor: e-resize; user-select: none;"><span></span><div style="cursor: pointer;" class="splitbuttonV" unselectable="on"></div></div>
			<div class="content spinner_large" style="width: 0px;"> <!-- The content frame was default with a spinner -->
			</div>
    	</div>
	</div>
	<div id="single-area" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;" class="spinner_large">
		<iframe name="single_content_iframe" id="single_content_iframe" src="/kzhtIndex" __idm_frm__="6442450947" style="height: 588.4px;"></iframe>
	</div>
	<div id="aboutModal" class="modal hide fade" style="width:875px;max-height:650px;margin:0pt 0pt 0pt -437px;">
		<div id="about_modal_header" class="modal-header">
			<a href="#" class="close">×</a>
			<h3>关于亚信安全服务器深度安全防护系统</h3>
		</div>
		<div id="about_modal_body" class="modal-body" style="max-height:none;">
			<div class="control_border" id="about">
				<div style="padding: 18px;">
					<table>
						<tbody>
							<tr>
								<td style="vertical-align: top;" width="60">
									<img src="/kzht/manager.svg" width="48" height="48">
								</td>
								<td height="60">
									<b>
									亚信安全服务器深度安全防护系统管理中心
									 
										(版本 10.5.5479)
									
									<br>
									版权所有 © 2017 亚信科技（成都）有限公司。保留所有权利
									</b>
									<br><br><br>
									Microsoft、Windows、Windows Vista、Windows Server、SQL Server 和 Active Directory 是 Microsoft Corporation 的注册商标。<br><br>Novell 和 eDirectory 是 Novell, Inc. 的注册商标。<br><br>Oracle、Solaris 和 Java 是 Oracle 和/或其附属公司的注册商标。<br><br>VMware、VMware vSphere、ESXi Server 和“框”徽标及设计是 VMware, Inc. 的注册商标。<br><br>Red Hat 是 Red Hat, Inc. 的注册商标。<br><br>SUSE 是 Novell, Inc. 的注册商标。<br><br>Linux 是 Linus Torvalds 的注册商标。<br><br>此处提及的所有其他标记和名称可能为各自公司的商标。
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	







	
	<!-- Navigation JS End -->



  

<div id="status_bar_container"><div id="status_bar" class="sb_left"><div class="status_bar_body"><div class="status_bar_content"><div><div id="sb_tasks" style="width: 1251.1px;"><div id="sb_tasks_area" class="sb_tasks_area" style="width: 1736px; left: 0px;"></div></div><div class="status_bar_item" id="sb_alerts"><span class="sb_alerts_title">警报</span><div class="sb_alert" id="sb_alert_warnings" style="border: 2px solid #eeb81c;" title="警告">262</div><div class="sb_alert" id="sb_alert_critical" style="border: 2px solid #DD3127;" title="严重">10</div></div></div></div></div></div><div id="sb_tasks_scroll" class="sb_tasks_scroll" style="left: 1313.1px;"><span id="sb_scroll_right" class="sb_scroll sb_scroll_right sb_scroll_disabled" style="display: none;"></span><span id="sb_scroll_left" class="sb_scroll sb_scroll_left sb_scroll_disabled" style="display: none;"></span></div></div></body></html>