<%@ page contentType="text/html;charset=utf-8" language="java" %>
<!DOCTYPE html>
<html  data-scrapbook-create="20211118054557599" data-scrapbook-title="亚信安全服务器深度安全防护系统管理中心 - 登录" data-scrapbook-type="site">
<head><meta charset="UTF-8">
		<title>亚信安全服务器深度安全防护系统管理中心 - 登录</title>
		<meta name="robots" content="noindex, nofollow">
		<meta http-equiv="imagetoolbar" content="no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--[if lt IE 9]>
		<script src="lib/bootstrap/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="/login/bootstrap-1065112file.csa.css">
	<link rel="stylesheet" type="text/css" href="/login/styles-zh_CN-1065112.csa.css">
	<link rel="stylesheet" type="text/css" href="/login/signin-1065112file.csa.css">

	</head>
	<body id="login_body">
		<form name="mainForm" method="post" action="/user/login" id="mainForm">
				<div id="login_form_container">
					<div id="login_form" class="clearfix">
						<div id="tm_header">
							<img src="/login/product_logo_zh_CN.svg" class="tm_logo">
							
								<a   href="#">
									<span class="icon_support">支持${msg}</span>
								</a>
							
						</div>
						<div id="login_tm_logo_container"><h2>登录</h2></div>
						
						<div id="login_error_container" class="alert-message warning hide" style="display: none;"><a id="login_error_close" class="close" href="#">×</a>
				    		<p>&nbsp;</p>
				    	</div>
				    	<div id="login_empty_container" class="hide alert-message error">
				    		<p>&nbsp;</p>
				    	</div>
						<div id="unsupported-browser-warning" class="hide alert-message warning">
							<p>亚信安全服务器深度安全防护系统管理中心不完全支持您的浏览器。建议使用最新版本的 Chrome、Firefox、Safari、Internet Explorer 或 Edge。</p>
						</div>
						

						<div id="normal" style="">
						

							<div id="username_box" class="clearfix input-box focused">
								<div class="input">
									<img src="/login/username.svg">
									<label class="floating-label" for="username">用户名</label>
					        		<input id="username" name="username" type="text" value="" class="" maxlength="254" autocomplete="off">
								</div>
							</div>
							<div id="password_box" class="clearfix input-box focused">
								<div class="input">
									<img src="/login/password.svg">
									<label class="floating-label" for="password">密码</label>
					            	<input id="password" name="password" type="password" class="" maxlength="128" autocomplete="off">
								</div>
							</div>
							<div id="username_box" class="clearfix input-box focused">
								<div class="input">
									<span style="display: inline">
    <input type="text" name="imageCode" placeholder="验证码" style="width: 50%;"/>
    <img src="/code/image"/ style="margin-left:35px;width:90px;height:30px;" >
</span>
								</div>
							</div>
							
							
							
							<div id="mfa_select_box" class="clearfix select-box checkbox-wrapper">
								<label for="haveMfa" style="color:red;vertical-align: bottom; width: 300px; float: none;">${msg}</label>&nbsp;
							</div>
							<% session.setAttribute("msg", "");%>

							<!-- <div id="mfa_select_box" class="clearfix select-box checkbox-wrapper">
								<input id="haveMfa" name="haveMfa" type="checkbox" value="true" style="float:none;">
								<label for="haveMfa" class="fake-checkbox"></label>
								<label for="haveMfa" style="vertical-align: bottom; width: 300px; float: none;">使用多重认证</label>&nbsp;
							</div> -->
							

							<input id="signinButton"   type="submit" value="登录" class="btn primary">
						</div>
					</div>
					

				</div>
 		<div class="formFooter" style="display:none">
</div>
</form>


		



<script type="text/javascript">

function s(){
	document.getElementById("mainForm").submit();
}
</script>
		
		


		
		<!-- Navigation JS End -->
		
	
    
    </body></html>